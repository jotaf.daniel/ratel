SCR=setup_static_ip.sh
UNT=staticip.service

serv:
	@chmod 700 ${SCR}
	@cp ${SCR} /root/${SCR}
	@systemctl enable --now ${UNT}
